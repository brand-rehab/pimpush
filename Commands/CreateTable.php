<?php

declare(strict_types=1);

namespace BrandRehab\PimPush\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use BrandRehab\PimPush\Database\Schema;

class CreateTable extends Command
{
  protected $options;
  protected $schema;
  protected $orm;

  protected function configure()
  {
    $this
    ->setName('create:table')
    ->setDescription('Creates a new table.')
    ->setHelp('This command creates a new table from the passed schema')
    ->addArgument('name', InputArgument::REQUIRED);
  }

  protected function setOptions($input, $output)
  {
    $raw = strtolower(preg_replace("/[^ \w]+/", "", $input->getArgument('name')));
    $raw_arr = explode(' ', $input->getArgument('name'));
    array_walk($raw_arr, function(&$value, &$key) {
        $value = ucwords($value);
    });
    $class = implode($raw_arr);

    $this->options = [
      'name' => [
        'raw' => str_replace(' ', '', $raw),
        'class' => $class.'Schema',
      ]
    ];
  }

  protected function setSchema(string $schema): void
  {
    $this->schema = new Schema($schema);
  }

  protected function execute(InputInterface $input, OutputInterface $output): void
  {
      $this->setOptions($input, $output);
      $name = $this->options['name'];
      $this->setSchema($name['raw']);
      $this->schema->createTable();
  }
}
