<?php

declare(strict_types=1);

namespace BrandRehab\PimPush\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateSchema extends Command
{
  protected $options;
  protected $root;

  protected function configure()
  {
    $this
    ->setName('create:schema')
    ->setDescription('Creates a new schema.')
    ->setHelp('This command creates a new table schema')
    ->addArgument('name', InputArgument::REQUIRED);
  }

  protected function setOptions($input, $output)
  {
    $raw = strtolower(preg_replace("/[^ \w]+/", "", $input->getArgument('name')));
    $raw_arr = explode(' ', $input->getArgument('name'));
    array_walk($raw_arr, function(&$value, &$key) {
        $value = ucwords($value);
    });
    $class = implode($raw_arr);

    $this->options = [
      'name' => [
        'raw' => str_replace(' ', '', $raw),
        'file' => $class.'Schema.php',
        'class' => $class.'Schema',
      ]
    ];
  }

  protected function setRoot()
  {
    $this->root = __DIR__ . '/../../../..';
  }

  protected function getTemplate($template)
  {
    return __DIR__.'/../Templates/'.$template;
  }

  protected function getSchema($schema)
  {
    return $this->root.'/database/schemas/'.$schema;
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
      $this->setOptions($input, $output);
      $this->setRoot();
      $template = file_get_contents($this->getTemplate('Schema.txt'));
      $name = $this->options['name'];
      $parsed_template = str_replace(['**raw**','**class**'],[$name['raw'],$name['class']],$template);
      file_put_contents($this->getSchema($name['file']), $parsed_template);
  }
}
