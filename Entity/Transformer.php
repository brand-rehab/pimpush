<?php
declare(strict_types=1);

namespace BrandRehab\PimPush\Entity;

use BrandRehab\PimPush\Entity\Model;

class Transformer
{
  protected $model;

  public function transform(model $model): ?array
  {
    if (!$this->classMatch($model)) return null;
    return $model->isCollection() ? $this->transformCollection($model) : $this->transformItem($model);
  }

  private function classMatch(model $model): bool
  {
    return $this->getClass() == get_class($model) ? true : false;
  }

  private function getClass(): string
  {
    return get_class($this->model);
  }

  private function transformItem(model $model): array
  {
    $data = $model->getItem();
    return $this->model($data);
  }

  private function transformCollection(model $model): array
  {
    $arr = [];
    foreach($model->getCollection() as $item) {
      $data = $item->getData();
      $arr[] = $this->model($data);
    }
    return $arr;
  }
}
