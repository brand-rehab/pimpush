<?php
declare(strict_types=1);

namespace BrandRehab\PimPush\Entity;

use \stdClass;
use BrandRehab\PimPush\Database\Orm;
use BrandRehab\PimPush\Database\Item;

class Model
{
  private $orm;

  public function __construct()
  {
    $this->orm = new Orm($this->defaultTable());
  }

  private function defaultTable(): string
  {
    return $this->propertyExists('table') ? $this->table : (new \ReflectionClass($this))->getShortName();
  }

  private function propertyExists(string $property_name): bool
  {
    return property_exists($this, $property_name);
  }

  public function getItem(): stdClass
  {
    return $this->orm->getItem();
  }

  public function createItem(): void
  {
    print_r($this->orm->columns());die();
    //$this->orm->createItem();
  }

  public function getCollection(): array
  {
    return $this->orm->getCollection();
  }

  public function isCollection(): bool
  {
    return $this->orm->isCollection();
  }

  public function first($value = null, ?string $field = 'id'): void
  {
    foreach ($this->orm->first($value, $field) as $key => $val) {
      $this->{$key} = $val;
    }
  }

  public function firstOrFail($value = null, ?string $field = 'id'): void
  {
    foreach ($this->orm->firstOrFail($value, $field) as $key => $val) {
      $this->{$key} = $val;
    }
  }

  public function all($value = null, ?string $field = 'id'): void
  {
    foreach ($this->orm->all($value, $field) as $key => $val) {
      $this->{$key} = $val;
    }
  }

  public function allOrFail($value = null, ?string $field = 'id'): void
  {
    foreach ($this->orm->allOrFail($value, $field) as $key => $val) {
      $this->{$key} = $val;
    }
  }

  public function columns(): array
  {
    return $this->orm->columns();
  }

  public function create(stdClass $properties): model
  {
    $this->orm->create();
    $assignable = new stdClass();
    foreach($properties as $key => $value) {
      if (in_array($key, $this->assigned)) {
        $assignable->{$key} = $value;
      }
    }
    $this->orm->update($assignable);
    return $this;
  }

  public function save(): int
  {
    return $this->orm->save();
  }
}
