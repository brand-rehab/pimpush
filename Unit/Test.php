<?php
declare(strict_types=1);

namespace BrandRehab\PimPush\Unit;

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use BrandRehab\PimPush\Http\Response;

class Test extends TestCase
{
  private $client;
  private $status_code;
  private $payload;

  public function get($uri)
  {
    if (!$this->hasClient()) $this->setClient();
    $this->setResponse($this->client->get($uri));
  }

  private function setResponse(Response $response)
  {
    $this->setStatusCode($response->getStatusCode());
    $this->setPayload($response->getBody());
  }

  private function setClient()
  {
    $config = require __DIR__ ."/../../../../config/app.php";
    $this->client = new Client(['base_uri' => $config["server"]["base_uri"]]);
  }

  private function hasClient(): bool
  {
    return ($this->client) ? true : false;
  }

  private function setStatusCode(string $status_code)
  {
    $this->status_code = $status_code;
  }

  public function getStatusCode(): string
  {
    return $this->status_code;
  }

  private function setPayload(?string $payload)
  {
    $this->payload = json_decode($payload);
  }

  public function getPayload(): ?string
  {
    return $this->payload;
  }
}
