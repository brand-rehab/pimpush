<?php
declare(strict_types=1);

namespace BrandRehab\PimPush\Http;

use BrandRehab\PimPush\Http\Request;

class Route
{
  private $controller_name;
  private $controller;
  private $request;

  public function __construct(Request $request)
  {
    $this->request = new Request();
  }

  private function controllerExists(): bool
  {
    return class_exists($this->controllerName()) ? true : false;
  }

  private function controllerName(): string
  {
    return $this->controller_name;
  }

  private function setControllerName(string $controller_name): void
  {
    $this->controller_name ="\\NuNu\\App\\Controllers\\".$controller_name;
  }

  public function setController(): void
  {
    $controller = $this->controllerName();
    $this->controller = new $controller();
  }

  private function hasArg(?string $arg): bool
  {
    return ($arg) ? true : false;
  }

  private function correctDepth(string $path): bool
  {
    return (substr_count($path, '/') === substr_count($this->request->uri(), '/')) ? true : false;
  }

  private function getArg(string $path, string $arg_name): ?string
  {
    $path_arr = explode('/', $path);
    $uri_arr = explode('/', $this->request->uri());

    for ($x=0; $x < count($path_arr); $x++) {
      if ($path_arr[$x] == '{'.$arg_name.'}') {
        return $uri_arr[$x];
      }
    }

    return null;
  }

  private function pathMatches(string $path): bool
  {
    return ($this->request->uri() === $path) ? true : false;
  }

  private function requestMethodMatches(string $method): bool
  {
    return ($this->request->method() === $method) ? true : false;
  }

  public function get(string $path, string $controller_name, ?string $arg_name = null): void
  {
    if (!$this->requestMethodMatches('get')) return;
    if (!$this->correctDepth($path)) return;

    $arg = ($this->hasArg($arg_name)) ? $this->getArg($path, $arg_name) : null;
    if (!$this->pathMatches(str_replace('{'.$arg_name.'}', $arg, $path))) return;

    $this->setControllerName($controller_name);
    if (!$this->controllerExists()) return;

    $this->setController();
    $this->controller->read($arg);
  }

  private function input()
  {

  }

  public function post(string $path, string $controller_name): void
  {
    if (!$this->requestMethodMatches('post')) return;
    if (!$this->correctDepth($path)) return;

    $payload = $this->request->post();
    if (empty($payload)) return;

    if (!$this->pathMatches($path)) return;

    $this->setControllerName($controller_name);
    if (!$this->controllerExists()) return;

    $this->setController();
    $this->controller->create($payload);
  }
}
