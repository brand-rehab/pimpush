<?php
declare(strict_types=1);

namespace BrandRehab\PimPush\Http;

class Status
{
  static $ok = 200;
  static $not_found = 404;
}
