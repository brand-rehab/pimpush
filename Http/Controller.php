<?php
declare(strict_types=1);

namespace BrandRehab\PimPush\Http;

use BrandRehab\PimPush\Http\Response;

class Controller
{
  protected $response;

  public function __construct()
  {
    $this->response = new Response();
  }
}
