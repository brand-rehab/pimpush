<?php
declare(strict_types=1);

namespace BrandRehab\PimPush\Http;

use \stdClass;

class Request
{
  public function __construct()
  {
    $get = !empty($_GET) ? json_decode(json_encode($_GET)) : null;
    $post = $this->processPostedData(file_get_contents("php://input"));

    $this->set('uri', $_SERVER['REQUEST_URI'])
      ->set('method', $_SERVER['REQUEST_METHOD'])
      ->set('get', $get)
      ->set('post', $post)
      ->set('query', $_SERVER['QUERY_STRING']);
  }

  public function set(string $property, $value): request
  {
    if ($this->has($property)) return $this;
    $this->{$property} = (gettype($value) == 'string') ? strtolower($value) : $value;
    return $this;
  }

  public function uri(): ?string
  {
    return $this->has('uri') ? $this->uri : null;
  }

  public function method(): ?string
  {
    return $this->has('method') ? $this->method : null;
  }

  public function post(): ?stdClass
  {
    return $this->has('post') ? $this->post : null;
  }

  private function has(string $key): bool
  {
    return property_exists($this, $key);
  }

  private function processPostedData(string $json_string): ?stdClass
  {
    return !empty($json_string) ? json_decode($json_string) : null;
  }
}
