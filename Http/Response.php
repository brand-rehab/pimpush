<?php
declare(strict_types=1);

namespace BrandRehab\PimPush\Http;

use \Twig_Loader_Filesystem as Twig_Loader_Filesystem;
use \Twig_Environment as Twig_Environment;
use \Twig_Extension_Debug as Twig_Extension_Debug;

class Response
{

  private $root;
  private $payload;
  private $template;
  private $status;
  private $twig;

  public function __construct()
  {
    $this->root = $_SERVER["DOCUMENT_ROOT"];
  }

  private function getTwigFileSystem(): string
  {
    return $this->root.'/templates';
  }

  private function getTwigCache(): string
  {
    return $this->root.'/cache/twig';
  }

  private function getJsManifest(): string
  {
    return $this->root.'/resources/dist/js/manifest.json';
  }

  private function getCssManifest(): string
  {
    return $this->root.'/resources/dist/css/manifest.json';
  }

  private function setPayload(?array $payload): response
  {
    $this->payload = (is_array($payload)) ? json_encode($payload) : null;
    return $this;
  }

  private function setStatus(int $status): response
  {
    $this->status = $status;
    return $this;
  }

  private function getPayload(): ?string
  {
    return $this->payload;
  }

  private function getTemplate(): ?string
  {
    return $this->template;
  }

  private function getStatus(): int
  {
    return $this->status;
  }

  private function getResponse(): void
  {
    http_response_code($this->getStatus());
    echo $this->getPayload();
    die();
  }

  private function setTwig(): response
  {
    $loader = new Twig_Loader_Filesystem($this->getTwigFileSystem());
    $this->twig = new Twig_Environment($loader, [
      'debug' => true,
      'auto_reload' => true,
      'cache' => $this->getTwigCache(),
    ]);
    $this->twig->addExtension(new Twig_Extension_Debug());
    return $this;
  }

  private function getManifests(): ?array
  {
    $manifests = [
      'js' => null,
      'css' => null,
    ];

    if (file_exists($this->getJsManifest())) {
      $manifests['js'] = json_decode(file_get_contents($this->getJsManifest()), true);
    }

    if (file_exists($this->getCssManifest())) {
      $manifests['css'] = json_decode(file_get_contents($this->getCssManifest()), true);
    }

    return $manifests;
  }

  private function getPage(string $template, ?array $vars): void
  {
    http_response_code($this->getStatus());
    $nunu = [
      'nunu' => [
        'meta' => [
          'status' => $this->getStatus(),
          'manifests' => $this->getManifests(),
        ],
        'data' => $vars,
      ]
    ];
    echo $this->twig->render('pages/'.$template.'.html.twig', $nunu);
    die();
  }

  public function json(int $status, ?array $payload = null): void
  {
    $this->setStatus($status)->setPayload($payload)->getResponse();
  }

  public function twig(int $status, string $template, ?array $vars = null): void
  {
    $this->setStatus($status)->setTwig()->getPage($template, $vars);
  }

  public function header(int $status, array $headers): void
  {
    http_response_code($status);
    switch ($status) {
      case 201:
        header("Location: {$headers['location']}");
        break;
    }
    die();
  }
}
