<?php
declare(strict_types=1);

namespace BrandRehab\PimPush\Middleware;

class Jwt
{
  private $request;

  public function __construct(request $request): void
  {
    $this->request = $request;
  }

  public function process()
  {
    /**
     * @todo check config for Cors
     */
  }
}
