<?php
declare(strict_types=1);

namespace BrandRehab\PimPush\Middleware;

use BrandRehab\PimPush\Http\Request;

class Cors
{
  private $request;

  public function __construct(request $request): void
  {
    $this->request = $request;
  }

  public function process()
  {
    /**
     * @todo check config for Cors
     */
  }
}
