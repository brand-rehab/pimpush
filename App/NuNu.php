<?php
declare(strict_types=1);

namespace BrandRehab\PimPush\App;

use BrandRehab\PimPush\Http\Request;
use BrandRehab\PimPush\Http\Route;

class NuNu
{
  private $route;
  private $request;
  private $root;

  public function __construct()
  {
    $this->request = new Request();
    $this->route = new Route($this->request());
    $this->root = $_SERVER["DOCUMENT_ROOT"];
  }

  public function run(): void
  {
    require $this->root()."/routes/api.php";
  }

  public function root(): string
  {
    return $this->root;
  }

  public function route(): route
  {
    return $this->route;
  }

  public function request(): request
  {
    return $this->request;
  }
}
