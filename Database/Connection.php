<?php
declare(strict_types=1);

namespace BrandRehab\PimPush\Database;

use \PDO;

final class Connection
{
  private static $connection;
  private static $db;

  private function __construct(){}

  public static function init(): pdo
  {
    if ( !isset( self::$connection ) ) {
      self::connect();
    }
    return self::$connection;
  }

  private static function connect(): void
  {
    $config = require __DIR__ ."/../../../../config/app.php";
    self::$db = $config["database"];
    self::$connection = new PDO("mysql:host=".self::$db['host'].";dbname=".self::$db['name'], self::$db['user'], self::$db['password'], [
      PDO::ATTR_PERSISTENT => true,
      PDO::ATTR_EMULATE_PREPARES => false
    ]);
  }

  public static function dbName(): string
  {
    if ( !isset( self::$db ) ) {
      self::connect();
    }
    return self::$db['name'];
  }

  public function __clone()
  {
    return false;
  }

  public function __wakeup()
  {
    return false;
  }
}
