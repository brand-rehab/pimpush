<?php
declare(strict_types=1);

namespace BrandRehab\PimPush\Database;

use \PDO;
use \stdClass;

class Item
{
  private $_data;
  private $_found;

  public function __construct ($stmt)
  {
    if ($stmt) {
      if (get_class($stmt) == "PDOStatement") {
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_OBJ);
      } else {
        $result = $stmt;
      }
    } else {
      $result = false;
    }

    $this->setFound($result ? true : false);
    if (!$this->isFound()) return false;

    $this->setData($result);
    $this->setParams($result);
  }

  private function setFound(bool $found): void
  {
    $this->found = $found;
  }

  public function isFound(): bool
  {
    return $this->found;
  }

  public function setData(stdClass $data): void
  {
    $this->_data = $data;
  }

  public function getData(): stdClass
  {
    return $this->_data;
  }

  public function setParams(stdClass $params): void
  {
    foreach ($params as $k => $v) {
      $this->$k = $v;
    }
  }

  public function getParam(string $param)
  {
    return $this->$param;
  }

  public function __toString(): stdClass
  {
    return $this->_data;
  }
}
