<?php
declare(strict_types=1);

namespace BrandRehab\PimPush\Database;

use \PDO;
use BrandRehab\PimPush\Database\Item;

class Collection
{
  private $_data;
  private $_found;

  public function __construct ($stmt)
  {
    $stmt->execute();
    $results = $stmt->fetchAll(PDO::FETCH_OBJ);

    $this->setFound($results ? true : false);
    if (!$this->isFound()) return false;

    $this->setData($results);
  }

  private function setData(array $data): void
  {
    foreach($data as $item) {
      $this->_data[] = new Item($item);
    }
  }

  public function getData(): array
  {
    return $this->_data;
  }

  private function setFound(bool $found): void
  {
    $this->found = $found;
  }

  public function isFound(): bool
  {
    return $this->found;
  }

  public function __toString(): array
  {
    return $this->getData();
  }
}
