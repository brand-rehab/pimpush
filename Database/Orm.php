<?php
declare(strict_types=1);

namespace BrandRehab\PimPush\Database;

use \PDO;
use \PDOStatement;
use \stdClass;
use BrandRehab\PimPush\Database\Connection;
use BrandRehab\PimPush\Database\Item;
use BrandRehab\PimPush\Database\Collection;
use BrandRehab\PimPush\Database\Schema;
use BrandRehab\PimPush\Exception\Handler;

class Orm
{
  protected $db;
  protected $item;
  protected $collection;
  protected $table;
  protected $schema;
  protected $connection;

  protected $limit;
  protected $where;

  public function __construct($model_name)
  {
    $this->table = $model_name.'s';
    $this->connect();
    $this->schema($model_name);
  }

  private function connect(): void
  {
    $this->connection = Connection::init();
  }

  private function schema(string $model_name): void
  {
    $this->schema = new Schema($model_name);
  }

  public function getCollection(): array
  {
    return $this->collection->getData();
  }

  public function getItem(): stdClass
  {
    return $this->item->getData();
  }

  public function isCollection(): bool
  {
    return $this->collection ? true : false;
  }

  public function isItem(): bool
  {
    return $this->item ? true : false;
  }

  public function limit(int $limit): orm
  {
    if (!$this->limit) $this->limit = " LIMIT {$limit}";
    return $this;
  }

  public function where(string $field, $value, ?string $operator = '='): orm
  {
    $where = $this->where ? "{$this->where} AND " : " (";
    $column = $this->columnType($field);
    if (!$column) return null;
    if ($this->isStringType($column['type'])) {
      $value = "`{$value}`";
    };
    $this->where = "{$where}`{$field}`{$operator}{$value}";
    return $this;
  }

  public function or(string $field, $value, ?string $operator = '='): orm
  {
    $column = $this->columnType($field);
    if (!$column) return null;
    if ($this->isStringType($column['type'])) {
      $value = "'{$value}'";
    };
    if ($this->where) $this->where = "{$this->where}) OR (`{$field}`{$operator}{$value}";
    return $this;
  }

  private function isStringType(string $column_type): bool
  {
    $string_types = [
      'char',
      'enum',
      'varchar',
      'tinytext',
      'text',
      'mediumtext',
      'longtext',
    ];
    return in_array($column_type, $string_types) ? true : false;
  }

  private function columnType(string $column): ?array
  {
    $columns = $this->columnTypes();
    return isset($columns[$column]) ? $columns[$column] : null;
  }

  private function columnTypes(): array
  {
    return $this->schema->columnTypes();
  }

  public function create(): void
  {
    $this->item = new Item(false);
  }

  public function update(stdClass $data): void
  {
    $this->item->setData($data);
    $this->item->setParams($data);
  }

  private function insert(stdClass $fields): array
  {
    $columns = null;
    foreach($fields as $field => $value) {
      $column = $this->columnType($field);
      if (!$column) continue;
      if ($this->isStringType($column['type'])) {
        $value = "'{$value}'";
      }
      $columns["`$field`"] = $value;
    }

    if ($columns) {
      $columns["`created_at`"] = 'CURRENT_TIMESTAMP';
    }

    return $columns;
  }

  public function save(): int
  {
    $data = $this->item->getData();
    $fields = $this->insert($data);
    $columns = implode(', ', array_keys($fields));
    $values = implode(', ', $fields);
    $stmt = "INSERT INTO `{$this->table}` ({$columns}) VALUES ({$values})";
    $stmt = $this->connection->prepare($stmt);
    $result = $stmt->execute();
    return (int) $this->connection->lastInsertId();
  }

  public function first($value, ?string $field = 'id'): item
  {
    return $value ? $this->where($field, $value)->limit(1)->find(false) : $this->limit(1)->find(false);
  }

  public function firstOrFail($value = null, ?string $field = 'id'): item
  {
    return $value ? $this->where($field, $value)->limit(1)->findOrFail(false) : $this->limit(1)->findOrFail(false);
  }

  public function all($value = null, ?string $field = 'id'): collection
  {
    return $value ? $this->where($field, $value)->find(true) : $this->find(true);
  }

  public function allOrFail($value = null, ?string $field = 'id'): collection
  {
    return $value ? $this->where($field, $value)->findOrFail(true): $this->findOrFail(true);
  }

  private function findItem(PDOStatement $stmt): item
  {
    $this->item = new Item($stmt);
    return $this->item;
  }

  private function findCollection(PDOStatement $stmt): collection
  {
    $this->collection = new Collection($stmt);
    return $this->collection;
  }

  private function find(?bool $collection = false)
  {
    $stmt = $this->select();
    return $collection ? $this->findCollection($stmt) : $this->findItem($stmt);
  }

  private function findOrFailItem(): item
  {
    if (!$this->item->isFound()) {
      $e = new Handler();
      $e->response('not_found');
    }
    return $this->item;
  }

  private function findOrFailCollection(): collection
  {
    if (!$this->collection->isFound()) {
      $e = new Handler();
      $e->response('not_found');
    }
    return $this->collection;
  }

  private function findOrFail(?bool $collection = false)
  {
    $this->find($collection);
    return $collection ? $this->findOrFailCollection() : $this->findOrFailItem();
  }

  private function select(): PDOStatement
  {
    $where = $this->where ? " WHERE {$this->where})" : null;
    $stmt = "SELECT * FROM `{$this->table}`{$where}{$this->limit}";
    return $this->connection->prepare($stmt);
  }
}
