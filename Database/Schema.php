<?php
declare(strict_types=1);

namespace BrandRehab\PimPush\Database;

use BrandRehab\PimPush\Database\Connection;
use \PDO;

class Schema
{
  public $schema;
  public $connection;
  public $dbname;

  public function __construct($model_name)
  {
    $schema_name = ucwords($model_name).'Schema';
    $schema_class = "\\NuNu\\Database\\Schemas\\{$schema_name}";
    $this->schema = new $schema_class();
  }

  public function columns(): array
  {
    return array_keys($this->columnTypes());
  }

  public function columnTypes(): array
  {
    return $this->schema->getColumns();
  }

  private function connect(): void
  {
    $this->connection = Connection::init();
  }

  private function dbName(): void
  {
    $this->dbname = Connection::dbName();
  }

  private function tableExists(string $database_name, string $table_name): bool
  {
    $stmt = "SELECT Table_Name from information_schema.TABLES where Table_Name='{$table_name}' and TABLE_SCHEMA='{$database_name}'";
    $stmt = $this->connection->prepare($stmt);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_OBJ);
    return $result ? true : false;
  }

  public function deleteTable(string $table_name): void
  {
    $stmt = "DROP TABLE `{$table_name}`";
    $stmt = $this->connection->prepare($stmt);
    $stmt->execute();
  }

  private function createVarchar(string $name, array $definition): string
  {
    return "`{$name}` varchar({$definition['length']}) DEFAULT NULL";
  }

  private function createEnum(string $name, array $definition): string
  {
    $enum = implode(',',$definition['length']);
    return "`{$name}` enum({$enum}) DEFAULT NULL";
  }

  private function createTinyInt(string $name, array $definition): string
  {
    return "`{$name}` tinyint(1) DEFAULT 0";
  }

  private function createRelationship(string $name): string
  {
    return "`{$name}` int(11) unsigned NOT NULL";
  }

  private function createColumns(array $column_definitions): ?string
  {
    $columns = null;
    foreach($column_definitions as $name => $definition) {
      switch ($definition['type']) {
        case 'varchar':
          $columns[] = $this->createVarchar($name, $definition);
          break;
        case 'enum':
          $columns[] = $this->createEnum($name, $definition);
          break;
        case 'tinyint':
          $columns[] = $this->createTinyInt($name, $definition);
          break;
        case 'relationship':
          $columns[] = $this->createRelationship($name);
          break;
      }
    }

    return $columns ? implode(',', $columns).',' : null;
  }

  public function createTable(): void
  {
    $this->connect();
    $this->dbName();
    $table_name = $this->schema->getTableName().'s';
    $database_name = $this->dbname;

    if ($this->tableExists($database_name, $table_name)) {
      $this->deleteTable($table_name);
    }

    $columns = $this->createColumns(array_slice($this->columnTypes(),1));

    $stmt = "CREATE TABLE `{$table_name}` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,{$columns}" .
      "`created_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()," .
      "`updated_at` timestamp NULL DEFAULT NULL," .
      "`deleted_at` timestamp NULL DEFAULT NULL," .
      "PRIMARY KEY (`id`)" .
      ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;";

    $stmt = $this->connection->prepare($stmt);
    $stmt->execute();
  }
}
