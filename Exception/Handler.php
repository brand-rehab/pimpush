<?php
declare(strict_types=1);

namespace BrandRehab\PimPush\Exception;

use BrandRehab\PimPush\Http\Response;
use BrandRehab\PimPush\Http\Status;

class Handler
{
  protected $status;
  protected $response;

  public function __construct()
  {
    $this->status = new Status();
    $this->response = new Response();
  }

  public function response(string $status, ?string $payload = null): void
  {
    $status_code = $this->status::$$status;
    $this->response->json($status_code, $payload);
  }
}
